# What is ReleafBrain?

ReleafBrain is a database editor and presentation toolkit. Its goal is to connect relief aid directly to where it is needed most.

It is being developed in cooperation with [Green Releaf](https://greenreleaf.org).

The first use case is the aftermath of hurricane Rai/Odette in the Philippines.

## Dependencies

### FoodBrain

FoodBrain is a similar database, but intended for plants and trees. To cut down on development time, generic parts from FoodBrain (such as contact and location data) are being reused in ReleafBrain.

### MODX

This software is distributed as an extra for MODX, a content management framework based on PHP and MySQL. Although the database model could in theory be used in other MySQL-based applications (with Directus, for example), for now it relies on MODX for providing the interface parts.

You need to have at least the following extras installed in MODX:

- pdoTools
- MIGX
- ImagePlus
- pThumb
- SuperBoxSelect

The following paid extras are optional, but recommended:

- Agenda (for managing food related events)
- ContentBlocks (for creating responsive web pages)
- Redactor (the rich text editor of choice)

### Semantic UI

A front-end framework called Fomantic UI is used for displaying ReleafBrain elements on a website. You can of course choose to roll out your own HTML/CSS, but with Fomantic UI this will be plug and (dis)play.

If it's not your intention to display your data on a website, then you don't need to worry about any HTML/CSS. The interface for managing data is provided by MODX and MIGX.

### Romanesco

If you _are_ planning to use the data on a website, then you might want to take a look at Romanesco. Romanesco is a collection of tools for prototyping and building websites. It integrates a front-end pattern library (using Semantic UI) directly into the CMS (MODX).

So if you install Romanesco, everything you need to use ReleafBrain is already included. Romanesco is also open source and free to download and use.

See https://romanesco.info for more information.

## License

ReleafBrain is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

[GNU General Public License v3](https://gitlab.com/fractal-farming/releafbrain/blob/master/core/components/releafbrain/docs/license.md)