<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package releafbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('releafbrain.core_path', null, $modx->getOption('core_path') . 'components/releafbrain/') . 'model/';
            
            $modx->addPackage('releafbrain', $modelPath, null);


            $manager = $modx->getManager();

            $manager->createObjectContainer('releafNeed');
            $manager->createObjectContainer('releafOffer');
            $manager->createObjectContainer('releafMatch');
            $manager->createObjectContainer('releafOrganization');
            $manager->createObjectContainer('releafNetwork');
            $manager->createObjectContainer('releafNetworkNode');
            $manager->createObjectContainer('releafNode');

            break;
    }
}

return true;