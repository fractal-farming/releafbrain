<?php
/**
 * @package ReleafBrain
 */

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

use Seld\JsonLint\JsonParser;

class ReleafBrain
{
    /**
     * A reference to the modX instance
     * @var modX $modx
     */
    public modX $modx;

    /**
     * The namespace
     * @var string $namespace
     */
    public string $namespace = 'releafbrain';

    /**
     * A configuration array
     * @var array $config
     */
    public array $config = [];

    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('releafbrain.core_path', $config, $this->modx->getOption('core_path') . 'components/releafbrain/');
        $assetsUrl = $this->modx->getOption('releafbrain.assets_url', $config,$this->modx->getOption('assets_url') . 'components/releafbrain/');

        $this->config = array_merge([
            'basePath' => $this->modx->getOption('base_path'),
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
        ], $config);

        $this->modx->loadClass('releafNeed', $this->config['modelPath'] . 'releafbrain/');

        $this->modx->addPackage('releafbrain', $this->config['modelPath']);
    }
}
