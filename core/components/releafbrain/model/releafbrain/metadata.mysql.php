<?php

$xpdo_meta_map = array (
  'xPDOSimpleObject' => 
  array (
    0 => 'releafNeed',
    1 => 'releafOffer',
    2 => 'releafMatch',
    3 => 'releafOrganization',
    4 => 'releafNetwork',
    5 => 'releafNode',
    6 => 'releafNetworkNode',
  ),
  'releafNode' => 
  array (
    0 => 'releafNodePerson',
  ),
  'earthImage' => 
  array (
    0 => 'releafImageNeed',
    1 => 'releafImageOffer',
    2 => 'releafImageMatch',
    3 => 'releafImageNode',
  ),
  'earthLink' => 
  array (
    0 => 'releafLinkNeed',
    1 => 'releafLinkOffer',
    2 => 'releafLinkNode',
  ),
  'earthNote' => 
  array (
    0 => 'releafNoteNeed',
    1 => 'releafNoteOffer',
    2 => 'releafNoteMatch',
    3 => 'releafNoteNode',
  ),
);