<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafNoteOffer']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'extends' => 'earthNote',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Offer' => 
    array (
      'class' => 'releafOffer',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
