<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafNetworkNode']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'table' => 'releafbrain_networks_nodes',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'network_id' => 0,
    'node_id' => 0,
  ),
  'fieldMeta' => 
  array (
    'network_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'node_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'network_node' => 
    array (
      'alias' => 'network_node',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'network_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'node_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Network' => 
    array (
      'class' => 'releafNetwork',
      'local' => 'network_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Node' => 
    array (
      'class' => 'releafNode',
      'local' => 'node_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
