<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafLinkNeed']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'extends' => 'earthLink',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Need' => 
    array (
      'class' => 'releafNeed',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Donate' => 
    array (
      'class' => 'releafNeed',
      'local' => 'id',
      'foreign' => 'link_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
  ),
);
