<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafImageNeed']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'extends' => 'earthImage',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Need' => 
    array (
      'class' => 'releafNeed',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Cover' => 
    array (
      'class' => 'releafNeed',
      'local' => 'id',
      'foreign' => 'image_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
  ),
);
