<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafLinkNode']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'extends' => 'earthLink',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Node' => 
    array (
      'class' => 'releafNode',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
