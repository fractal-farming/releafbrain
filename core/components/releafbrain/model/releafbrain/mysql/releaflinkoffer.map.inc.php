<?php
/**
 * @package ReleafBrain
 */
$xpdo_meta_map['releafLinkOffer']= array (
  'package' => 'releafbrain',
  'version' => '1.1',
  'extends' => 'earthLink',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Offer' => 
    array (
      'class' => 'releafOffer',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
