<?php

// Map
// =====================================================================

$_lang['releafbrain.map.button_read_more'] = "More info";
$_lang['releafbrain.map.location_obfuscated_heading'] = "This is not the exact location.";
$_lang['releafbrain.map.location_obfuscated_content'] = "For privacy / security reasons, the marker has been placed randomly within a [[+radius]] km radius.";

// Location
// =====================================================================

$_lang['releafbrain.location.heading'] = "Location";
$_lang['releafbrain.location.elevation'] = "Elevation";

// Address
// =====================================================================

$_lang['releafbrain.address.heading'] = "Address";

// Needs & Offers
// =====================================================================

$_lang['releafbrain.need.heading'] = "Request";
$_lang['releafbrain.need.quantity'] = "Quantity";
$_lang['releafbrain.need.category'] = "Category";
$_lang['releafbrain.need.person'] = "Person";
$_lang['releafbrain.need.organization'] = "Organization";
$_lang['releafbrain.need.respond'] = "I can help";

$_lang['releafbrain.need.community_size'] = "Community size";
$_lang['releafbrain.need.community_needs'] = "Our community needs:";

$_lang['releafbrain.offer.pledge'] = "Offer help";
$_lang['releafbrain.offer.donate'] = "Donate";