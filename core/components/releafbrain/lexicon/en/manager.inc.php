<?php

// Menus
// =====================================================================

$_lang['releafbrain.menu.clear_cache_maps_title'] = "Map data";
$_lang['releafbrain.menu.clear_cache_maps_description'] = "Refresh output of all map views";

// Collections
// =============================================================================

$_lang['collections.organization.children'] = "Organizations";
$_lang['collections.organization.children.create'] = "Add organization";
$_lang['collections.organization.children.back_to_collection_label'] = "Back to overview";

$_lang['collections.network.children'] = "Networks";
$_lang['collections.network.children.create'] = "Add network";
$_lang['collections.network.children.back_to_collection_label'] = "Back to overview";
