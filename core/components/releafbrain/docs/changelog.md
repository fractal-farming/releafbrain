# Changelog for ReleafBrain

## ReleafBrain 0.4.1
Released on August 6, 2024

Fixes and improvements:
- Segment overview cache based on user access level
- Extend Node object with releafNodePerson (WIP)
- Avoid empty popup content breaking geoJSON output
- Remove radius field from location configs (now a privacy setting)

## ReleafBrain 0.4.0
Released on March 10, 2023

New features:
- Add snippet for importing GeoJSON data
- Add basic templating engine + watcher for MIGX configs
- Reference cover image and donation link in parent object
- Add processors for creating Need, Node and Organization objects

Fixes and improvements:
- Add Node update processor (WIP)
- Add MIGX TV for geocoding address (WIP)
- Use spaces as separator for uploaded image paths
- Use merge-json.php script from EarthBrain
- Improvements to network-node relations
- Finalize Networks and Nodes grids
- Tweaks and fixes to Organizations grids
- Remove comments field from Match table
- Remove message field from Offer table
- Save original Need request as note
- Add overview template for showing parent need in form
- Show map marker icon in grid when Need has coordinates
- Add button for responding to a Need
- Fix broken output in Needs table
- Move query for fetching cover image to EarthBrain function
- Various fixes and tweaks to Need overviews
- Make images work in Need overviews
- Add image preview to Needs grid
- Replace / remove hardcoded access tokens

## ReleafBrain 0.3.1
Released on December 6, 2022

New features:
- Add Nodes to map
- Activate geocoding

Fixes and improvements:
- Update data extract with latest tables
- Save links and contact info to new Need
- Attach uploaded images to new Need
- Add menu item for clearing map data cache
- Segment custom caching partitions

## ReleafBrain 0.3.0
Released on November 22, 2022

New features:
- Add Networks and network Nodes
- Add Organizations

Fixes and improvements:
- Attach Images and Notes to various objects
- Add Mapper selection to Needs grid
- Add Organization selection to Needs grid
- Validate and process Need mapper data
- Make sure Need item can't select itself as parent
- Create separate Needs for each category
- Add parent-child relationship to Needs
- Add form hook for saving Need request directly to database
- Rename Transactions to Matches
- Rename Donations to Offers
- Rename Requests to Needs
- Rename Aid prefixes to Releaf
- Replace FoodBrain with EarthBrain

## ReleafBrain 0.2.0
Released on February 12, 2022

New features:
- Show aid request data in table
- Import aid request data from Kobo

Fixes and improvements:
- Rename aidItem to aidDonation

## ReleafBrain 0.1.0
Released on January 22, 2022

Initial project setup:
- Add MIGXdb configs
- Add database schema
