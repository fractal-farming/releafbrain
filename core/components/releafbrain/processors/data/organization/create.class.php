<?php
class releafOrganizationCreateProcessor extends modResourceCreateProcessor
{
    public $classKey = 'modResource';
    public $languageTopics = array('releafbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    /** @var ReleafBrain $releafbrain */
    public $releafbrain;

    private array $earthProcessorProps;
    private array $releafProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('releafbrain.core_path', null, $this->modx->getOption('core_path') . 'components/releafbrain/');
        $this->releafbrain = $this->modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));

        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];
        $this->releafProcessorProps = ['processors_path' => $this->releafbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        if ($this->getProperty('pagetitle'))
        {
            // Check if organization already exists
            $query = $this->modx->newQuery('modResource', [
                'pagetitle' => $this->getProperty('pagetitle'),
                'context_key' => $this->getProperty('context_key'),
            ]);
            // Todo: insert where clause for finding approximate matches too.
            $query->select('id');
            $orgID = $this->modx->getValue($query->prepare());

            /**
             * !!! HACKERMAN ALERT !!!
             *
             * This is returned as error message, because it seems impossible to
             * output the regular object at this point. Use with caution, and
             * reset modError after retrieving the message! Otherwise, following
             * processes might think there's an error, and fail.
             */
            if ($orgID) {
                return $this->success('', ['id' => $orgID]);
            }
        }
        else {
            $this->addFieldError('pagetitle', 'Pagetitle not set!');
        }

        return parent::beforeSet();
    }

    public function afterSave()
    {
        // Create extended object
        $releafOrgData = $this->modx->newObject('releafOrganization');
        $releafOrgData->fromArray($this->getProperties());
        $releafOrgData->set('resource_id', (int)$this->object->get('id'));
        $releafOrgData->save();

        return parent::afterSave();
    }
}
return 'releafOrganizationCreateProcessor';