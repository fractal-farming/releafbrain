<?php
class releafNeedCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'releafNeed';
    public $languageTopics = array('releafbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    /** @var ReleafBrain $releafbrain */
    public $releafbrain;
    
    /** @var Romanesco $romanesco */
    public $romanesco;

    private array $earthProcessorProps;
    private array $releafProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('releafbrain.core_path', null, $this->modx->getOption('core_path') . 'components/releafbrain/');
        $this->releafbrain = $this->modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('romanescobackyard.core_path', null, $this->modx->getOption('core_path') . 'components/romanescobackyard/');
        $this->romanesco = $this->modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];
        $this->releafProcessorProps = ['processors_path' => $this->releafbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function afterSave()
    {
        $needAddress = $this->getProperty('needAddress');
        $needLocation = $this->getProperty('needLocation');
        $orgData = $this->getProperty('orgData');
        $personData = $this->getProperty('personData');
        $personAddress = $this->getProperty('personAddress');
        $personLocation = $this->getProperty('personLocation');

        // Add person, if no existing data was found
        if ($personData['new'] && !$this->getProperty('createdby'))
        {
            // Add address and location to array
            $personData['personAddress'] = $personAddress;
            $personData['personLocation'] = $personLocation;

            $response = $this->modx->runProcessor('data/person/create', $personData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
            $person = $response->getObject();

            // Set createdby in other objects
            $needAddress['createdby'] = $person['id'];
            $needLocation['createdby'] = $person['id'];
            $orgData['createdby'] = $person['id'];

            // And this object too
            $this->object->set('createdby', $person['id']);
        }

        // Add organization, or fetch existing
        // @todo: This should obviously not use the create processor to fetch existing objects
        $response = $this->modx->runProcessor('data/organization/create', $orgData, $this->releafProcessorProps);
        if ($response->isError()) {
            // Look for hidden success, where the organization is already created
            if (isset($response->response['message']['success'])) {
                $organization['id'] = $response->response['message']['object']['id'];
                $this->modx->error->reset(); // Thanks HACKERMAN!
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
        } else {
            $organization = $response->getObject();
        }

        // Save address
        $response = $this->modx->runProcessor('data/address/create', $needAddress, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $address = $response->getObject();

        // Save location
        $response = $this->modx->runProcessor('data/location/create', $needLocation, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $location = $response->getObject();
        
        // Save images
        if ($this->getProperty('images')) {
            $parentID = $this->object->get('id');

            // Create destination folder
            $destPath = 'uploads/img/need/' . $parentID . '/';
            $this->romanesco->runCommand(['mkdir', MODX_BASE_PATH . $destPath]);

            // Image paths are provided as space-separated list
            $images = explode(' ', $this->getProperty('images'));
            $i = 0;

            foreach ($images as $image) {
                $srcPath = str_replace('/assets/../', '', $image);
                $i++;

                // Move image
                $this->romanesco->runCommand(['mv', MODX_BASE_PATH . $srcPath, MODX_BASE_PATH . $destPath]);

                // Create image object
                $imgData = [
                    'class_key' => 'releafImageNeed',
                    'parent_id' => $parentID,
                    'img' => '',
                    'title' => $this->getProperty('title') . " - $i",
                    'path' => $destPath . basename($srcPath),
                    'createdon' => $this->getProperty('createdon'),
                    'createdby' => $this->getProperty('createdby'),
                ];

                $response = $this->modx->runProcessor('data/image/create', $imgData, $this->earthProcessorProps);
                if ($response->isError()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                    return false;
                }
                $image = $response->getObject();

                // Set first image as cover image
                if ($i == 1) {
                    $this->object->set('image_id', $image['id']);
                }
            }
        }

        // Save links
        if ($this->getProperty('links')) {
            $parentID = $this->object->get('id');
            $links = explode("\n", $this->getProperty('links'));
            $links = array_unique($links);
            $links = array_filter($links);
            $i = 0;

            foreach ($links as $link) {
                $linkData = [
                    'class_key' => 'releafLinkNeed',
                    'parent_id' => $parentID,
                    'url' => $link,
                    'createdon' => $this->getProperty('createdon'),
                    'createdby' => $this->getProperty('createdby'),
                    'pos' => $i++,
                ];

                $response = $this->modx->runProcessor('data/link/create', $linkData, $this->earthProcessorProps);
                if ($response->isError()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                    return false;
                }
            }
        }

        // Save original message
        if ($this->getProperty('description')) {
            $parentID = $this->object->get('id');

            $content = $this->modx->getChunk('htmlOriginalMessage', [
                'title' => $this->getProperty('title'),
                'description' => $this->getProperty('description'),
            ]);

            $noteData = [
                'class_key' => 'releafNoteNeed',
                'parent_id' => $parentID,
                'title' => 'Original message',
                'content' => $content,
                'createdon' => $this->getProperty('createdon'),
                'createdby' => $this->getProperty('createdby'),
            ];

            $response = $this->modx->runProcessor('data/note/create', $noteData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
        }

        // Save local contact(s)
        if ($this->getProperty('contacts') && $this->getProperty('contact-details')) {
            $parentID = $this->object->get('id');

            $noteData = [
                'class_key' => 'releafNoteNeed',
                'parent_id' => $parentID,
                'title' => 'Local contact(s)',
                'content' => $this->getProperty('contact-details'),
                'createdon' => $this->getProperty('createdon'),
                'createdby' => $this->getProperty('createdby'),
            ];

            $response = $this->modx->runProcessor('data/note/create', $noteData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
        }

        // If request contains multiple categories, create separate entry for each
        $categories = (array)$this->getProperty('category');
        if ($categories && sizeof($categories) > 1) {
            $parentID = $this->object->get('id');

            foreach ($categories as $category) {
                $query = $this->modx->newQuery('rmOption', ['alias' => $category]);
                $query->select('name');
                $title = $this->modx->getValue($query->prepare());

                $needData = [
                    'parent_id' => $parentID,
                    'organization_id' => $organization['id'] ?? '',
                    'title' => $title,
                    'description' => '',
                    'message' => '',
                    'category' => $category,
                    'priority' => 2,
                    'quantity' => null,
                    'quantity_unit' => '',
                    'createdon' => $this->getProperty('createdOn'),
                    'createdby' => $this->getProperty('createdBy'),
                    'published' => 0,
                ];

                $needChild = $this->modx->newObject('releafNeed');
                $needChild->fromArray($needData);
                $needChild->save();
            }

            // Reset parent category
            $this->object->set('category', '');
        }
        else {
            $this->object->set('category', $categories[0]);
        }

        // Update need
        $this->object->set('address_id', $address['id']);
        $this->object->set('location_id', $location['id']);
        $this->object->set('organization_id', $organization['id'] ?? '');
        $this->object->save();

        return parent::afterSave();
    }
}
return 'releafNeedCreateProcessor';