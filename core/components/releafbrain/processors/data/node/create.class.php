<?php
class releafNodeCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'releafNode';
    public $languageTopics = array('releafbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    /** @var ReleafBrain $releafbrain */
    public $releafbrain;
    
    /** @var Romanesco $romanesco */
    public $romanesco;

    private array $earthProcessorProps;
    private array $releafProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('releafbrain.core_path', null, $this->modx->getOption('core_path') . 'components/releafbrain/');
        $this->releafbrain = $this->modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('romanescobackyard.core_path', null, $this->modx->getOption('core_path') . 'components/romanescobackyard/');
        $this->romanesco = $this->modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];
        $this->releafProcessorProps = ['processors_path' => $this->releafbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function afterSave()
    {
        $nodeAddress = $this->getProperty('nodeAddress');
        $nodeLocation = $this->getProperty('nodeLocation');
        $personData = $this->getProperty('personData');
        $personAddress = $this->getProperty('personAddress');
        $personLocation = $this->getProperty('personLocation');

        $nodeID = $this->object->get('id');

        // Add person, if no existing data was found
        if ($personData['new'] && !$this->getProperty('createdby'))
        {
            // Add address and location to array
            $personData['personAddress'] = $personAddress;
            $personData['personLocation'] = $personLocation;

            $response = $this->modx->runProcessor('data/person/create', $personData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
            $person = $response->getObject();

            // Set createdby in other objects
            $nodeAddress['createdby'] = $person['id'];
            $nodeLocation['createdby'] = $person['id'];

            // And this object too
            $this->object->set('createdby', $person['id']);
        }

        // Save address
        $response = $this->modx->runProcessor('data/address/create', $nodeAddress, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $address = $response->getObject();

        // Save location
        $response = $this->modx->runProcessor('data/location/create', $nodeLocation, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $location = $response->getObject();
        
        // Save images
        if ($this->getProperty('images'))
        {
            // Create destination folder
            $destPath = 'uploads/img/node/' . $nodeID . '/';
            $this->romanesco->runCommand(['mkdir', MODX_BASE_PATH . $destPath]);

            // Image paths are provided as space-separated list
            $images = explode(' ', $this->getProperty('images'));
            $i = 0;

            foreach ($images as $image) {
                $srcPath = str_replace('/assets/../', '', $image);
                $i++;

                // Move image
                $this->romanesco->runCommand(['mv', MODX_BASE_PATH . $srcPath, MODX_BASE_PATH . $destPath]);

                // Create image object
                $imgData = [
                    'class_key' => 'releafImageNode',
                    'parent_id' => $nodeID,
                    'img' => '',
                    'title' => $this->getProperty('title') . " - $i",
                    'path' => $destPath . basename($srcPath),
                    'createdon' => $this->getProperty('createdon'),
                    'createdby' => $this->getProperty('createdby'),
                ];

                $response = $this->modx->runProcessor('data/image/create', $imgData, $this->earthProcessorProps);
                if ($response->isError()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                    return false;
                }
                $image = $response->getObject();

                // Set first image as cover image
                if ($i == 1) {
                    $this->object->set('image_id', $image['id']);
                }
            }
        }

        // Save links
        if ($this->getProperty('links'))
        {
            $links = explode("\n", $this->getProperty('links'));
            $links = array_unique($links);
            $links = array_filter($links);
            $i = 0;

            foreach ($links as $link) {
                $linkData = [
                    'class_key' => 'releafLinkNode',
                    'parent_id' => $nodeID,
                    'url' => $link,
                    'createdon' => $this->getProperty('createdon'),
                    'createdby' => $this->getProperty('createdby'),
                    'pos' => $i++,
                ];

                $response = $this->modx->runProcessor('data/link/create', $linkData, $this->earthProcessorProps);
                if ($response->isError()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                    return false;
                }
            }
        }

        // Save original message
        if ($this->getProperty('description'))
        {
            $content = $this->modx->getChunk('htmlOriginalMessage', [
                'title' => $this->getProperty('title'),
                'description' => $this->getProperty('description'),
            ]);

            $noteData = [
                'class_key' => 'releafNoteNode',
                'parent_id' => $nodeID,
                'title' => 'Original message',
                'content' => $content,
                'createdon' => $this->getProperty('createdon'),
                'createdby' => $this->getProperty('createdby'),
            ];

            $response = $this->modx->runProcessor('data/note/create', $noteData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
        }

        // Save network details
        if ($this->getProperty('networks') && $this->getProperty('network-details'))
        {
            $noteData = [
                'class_key' => 'releafNoteNode',
                'parent_id' => $nodeID,
                'title' => 'Network details',
                'content' => $this->getProperty('network-details'),
                'createdon' => $this->getProperty('createdon'),
                'createdby' => $this->getProperty('createdby'),
            ];

            $response = $this->modx->runProcessor('data/note/create', $noteData, $this->earthProcessorProps);
            if ($response->isError()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return false;
            }
        }

        // Link node to network
        if ($this->getProperty('network_id')) {
            $nodeNetworkData = [
                'network_id' => $this->getProperty('network_id'),
                'node_id' => $nodeID,
            ];

            $nodeNetwork = $this->modx->newObject('releafNetworkNode');
            $nodeNetwork->fromArray($nodeNetworkData);
            $nodeNetwork->save();
        }

        // Update node
        // Todo: not sure if person_id should be the same as the mapper here
        $this->object->set('address_id', $address['id']);
        $this->object->set('location_id', $location['id']);
        $this->object->set('person_id', $person['id'] ?? $this->getProperty('createdby'));
        $this->object->save();

        return parent::afterSave();
    }
}
return 'releafNodeCreateProcessor';