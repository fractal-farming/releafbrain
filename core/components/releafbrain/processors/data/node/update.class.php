<?php
class releafNodeUpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'releafNode';
    public $languageTopics = array('releafbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    /** @var ReleafBrain $releafbrain */
    public $releafbrain;
    
    /** @var Romanesco $romanesco */
    public $romanesco;

    private array $earthProcessorProps;
    private array $releafProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('releafbrain.core_path', null, $this->modx->getOption('core_path') . 'components/releafbrain/');
        $this->releafbrain = $this->modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));
        $corePath = $this->modx->getOption('romanescobackyard.core_path', null, $this->modx->getOption('core_path') . 'components/romanescobackyard/');
        $this->romanesco = $this->modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];
        $this->releafProcessorProps = ['processors_path' => $this->releafbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        $nodeAddress = $this->getProperty('nodeAddress');
        $nodeLocation = $this->getProperty('nodeLocation');

        // Save address
        $response = $this->modx->runProcessor('data/address/update', $nodeAddress, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $address = $response->getObject();

        // Save location
        $response = $this->modx->runProcessor('data/location/update', $nodeLocation, $this->earthProcessorProps);
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
            return false;
        }
        $location = $response->getObject();

        $this->object->set('address_id', $address['id']);
        $this->object->set('location_id', $location['id']);

        return parent::beforeSet();
    }
}
return 'releafNodeUpdateProcessor';