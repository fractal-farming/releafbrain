<?php
/**
 * @var string $input
 */
switch($input) {
    case stripos($input,'ReleafNeedBasicCard') !== false:
    case stripos($input,'ReleafOfferBasicCard') !== false:
        $box_type = "cards";
        $row_type = "";
        $column_type = "card";
        $grid_settings = "";
        break;
    case stripos($input,'ReleafNeedBasicList') !== false:
    case stripos($input,'ReleafOfferBasicList') !== false:
        $box_type = "divided list";
        $row_type = "";
        $column_type = "item";
        $grid_settings = "";
        break;
    case stripos($input,'ReleafNeedWideCard') !== false:
        $box_type = "horizontal cards";
        $row_type = "";
        $column_type = "card";
        $grid_settings = "[[+de_emphasize:ne=`1`:then=`raised`]]";
        break;
    case stripos($input,'ReleafNeedWideSegment') !== false:
    case stripos($input,'ReleafNeedFormSegment') !== false:
        $box_type = "";
        $row_type = "";
        $column_type = "ui [[+padding:replace=`relaxed==padded`]] segment";
        $grid_settings = "[[+de_emphasize:ne=`1`:then=`raised`]]";
        break;
    case stripos($input,'ReleafNeedTable') !== false:
    case stripos($input,'ReleafOfferTable') !== false:
        $box_type = "sortable table";
        $row_type = "table";
        $column_type = "";
        $grid_settings = "small very compact [[+de_emphasize:eq=`1`:then=`very basic`]]";
        break;
    case stripos($input,'ReleafNeedBasic') !== false:
    case stripos($input,'ReleafOfferBasic') !== false:
        $box_type = "grid";
        $row_type = "";
        $column_type = "column";
        $grid_settings = "column";
        break;
    default:
        return '';
}

return [
    'box_type' => $box_type,
    'row_type' => $row_type,
    'column_type' => $column_type,
    'grid_settings' => $grid_settings,
];