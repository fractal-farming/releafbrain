<?php
/**
 * releafMapPrepareNeeds
 *
 * Modify field values before the pdoResources snippet in releafMapGetNeeds is
 * executed.
 *
 * Please note that the output of releafMapPrepareNeeds is a GeoJSON object, so
 * the output of each field needs to be valid JSON.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$tplPopupContent = $modx->getOption('tplPopupContent', $scriptProperties, 'earthMapPopupContent');

//$modx->log(modX::LOG_LEVEL_ERROR, '[releafMapPrepareNeeds] Row: ' . print_r($row, 1));
//$row = array();

// Coordinates
// =============================================================================

// Randomly generate nearby coordinates to obfuscate real location
if ($row['lat'] && $row['lng'] && $row['radius'] > 0) {
    $randomLocation = [];
    $randomLocation = $earthbrain->obfuscateCoordinates([$row['lat'],$row['lng']], $row['radius']);

    $row['lat'] = $randomLocation['lat'];
    $row['lng'] = $randomLocation['lng'];

    $row['message'] = $modx->getChunk('richTextMessage', [
        'size' => 'small',
        'message_type' => 'info',
        'heading' => $modx->lexicon('earthbrain.map.location_obfuscated_heading'),
        'content' => $modx->lexicon('earthbrain.map.location_obfuscated_content', ['radius'=>$row['radius']]),
    ]);
}


// Popup content
// =============================================================================

// Create buttons
$row['button_website'] = '';
$row['button_facebook'] = '';
if ($row['website']) {
    $row['button_website'] = $modx->getChunk('buttonHrefIcon', [
        'link' => $row['website'],
        'icon_class' => 'globe icon',
    ]);
}
if ($row['facebook']) {
    $row['button_facebook'] = $modx->getChunk('buttonHrefIcon', [
        'link' => $row['facebook'],
        'icon_class' => 'facebook f icon',
    ]);
}

// Render cover image with fixed media source path
$img = $row['img_wide'];
if ($img) {
    $img['class_key'] = 'releafImageNeed';
    $img['parent_id'] = $row['id'];
    $row['imageJSON'] = $earthimage->fixSourcePath($img);
}

// Generate image output
if ($row['imageJSON']) {
    // Create thumbnail with ImagePlus
    $image = $modx->runSnippet('ImagePlus', [
        'value' => $row['imageJSON'],
        'type' => 'thumb',
        'options' => 'w=640',
    ]);
    // Create HTML element
    $row['image'] = $modx->getChunk('earthMapPopupImage', [
        'image' => $image,
        'alt' => $row['pagetitle'],
    ]);
}

// Generate links
$row['link'] = $modx->makeUrl($row['id']);

// Format region
if ($row['locality'] && $row['region']) {
    $row['region'] = ', ' . $row['region'];
}

// Use chunk tpl for output
$row['popup_content'] = json_encode(
    $modx->getChunk($tplPopupContent, [
        'image' => $row['image'],
        'title' => $row['title'],
    ])
);

return json_encode($row);