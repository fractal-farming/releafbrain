<?php
/**
 * releafMapPrepareNeeds
 *
 * Modify field values before the pdoResources snippet in releafMapGetNeeds and
 * releafMapGetNeeds is executed.
 *
 * Please note that the final output is a GeoJSON object, so each field needs to
 * generate valid JSON.
 *
 * NB: in pdoTools, the geojson field returns an array!! So you need to encode
 * it first, or you'll see a null where [[+geojson]] is placed.
 * MigxLoopCollection does fetch a JSON string directly from the database, so
 * this can lead to some confusion.
 *
 * NB2: make sure you pretty print complex JSON output, as it may contain double
 * [[]] characters for nested arrays. This messes with MODX rendering (it's
 * interpreted as snippet, meaning no output).
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$tplPopupContent = $modx->getOption('tplPopupContent', $scriptProperties, 'earthMapFeaturePopupContent');

//$modx->log(modX::LOG_LEVEL_ERROR, '[releafMapPrepareOffers] Row: ' . print_r($row, 1));

// GeoJSON data
// =============================================================================

// Create geometry object
$row['geometry'] = '';

// Fill geometry object with GeoJSON data or coordinates
if ($row['geojson']) {
    $row['geometry'] = '"geometry": ' . json_encode($row['geojson']['geometry'],JSON_PRETTY_PRINT);
} else {
    $row['geometry'] = '"geometry": {
        "type": "Point",
        "coordinates": [ ' . $row['lng'] . ',' . $row['lat'] . ']
    }';
}

// Encode full GeoJSON object
if ($row['geojson']) {
    $row['geojson'] = json_encode($row['geojson'],JSON_PRETTY_PRINT);
}

// Popup content
// =============================================================================

$featureType = $modx->getObject('rmOption', array('alias' => $row['type']));
if ($featureType) $row['type'] = $featureType->get('name');

// Use chunk tpl for output
$row['popup_content'] = json_encode(
    $modx->getChunk($tplPopupContent, array(
        'description' => nl2br($row['description']),
        'type' => $row['type'],
    ))
);


return json_encode($row);