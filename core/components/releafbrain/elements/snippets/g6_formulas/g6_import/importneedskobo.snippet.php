<?php
/**
 * importKoboData snippet
 *
 * Get form submissions from Kobo Toolbox through the API.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$releafbrain = $modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($releafbrain instanceof ReleafBrain)) return;

// Guzzle should be available as MODX extra
if (!class_exists(Client::class)) {
    $modx->log(modX::LOG_LEVEL_ERROR, 'Guzzle client not found.');
    return 'Guzzle client not found.';
}
$client = new Client();

$apiURL = $modx->getOption('apiURL', $scriptProperties);
$apiUser = $modx->getOption('apiUser', $scriptProperties);
$apiKey = $modx->getOption('apiKey', $scriptProperties);
$cacheKey = $modx->getOption('cacheKey', $scriptProperties, 'earthbrain');

$cacheManager = $modx->getCacheManager();
$cacheLifetime = (int)$modx->getOption('cacheLifetime', $scriptProperties, 48 * 60 * 60, true);
$cacheOptions = [
    xPDO::OPT_CACHE_KEY => 'kobo',
];
$fromCache = true;
$data = $cacheManager->get($cacheKey, $cacheOptions);

// Connect to API and get the data
if (!is_array($data)) {
    $fromCache = false;
    $response = '';

    try {
        $response = $client->request('GET', $apiURL, [
            'headers' => [
                'Authorization' => 'Token ' . $apiKey,
                'Accept' => 'application/json',
            ],
        ]);
    }
    catch (GuzzleException $e) {
        if ($e->hasResponse()) {
            $response = $e->getResponse();
        }
    }

    if ($response->getStatusCode() != 200) {
        $error = '[importKoboData] Request failed with status code ' . $response->getStatusCode() . ': ' . $response->getBody();
        $modx->log(modX::LOG_LEVEL_ERROR, $error);
        return $error;
    }

    $data = json_decode($response->getBody(), true);
    $cacheManager->set($cacheKey, $data, $cacheLifetime, $cacheOptions);
}

//echo "<pre><code>";
//echo print_r($data['results'], 1);
//echo "</code></pre>";

if (!is_array($data)) {
    $modx->log(modX::LOG_LEVEL_ERROR, '[importKoboData] Could not find requested data');
    return '';
}

$features = [];

foreach ($data['results'] as $result) {
    $id = $result['_id'];
    $name = $result['request/title_other'] ?? $result['request/title'] ?? $result['needs/request_other'] ?? $result['needs/request'];
    $name = ucfirst($name);
    $description = $result['request/description'] ?? $result['needs/request_details'];
    
    $coordinates = $result['add_location/coordinates'] ?? $result['add_location/location'];
    $coordinates = explode(' ', $coordinates);
    $location = [
        'lat' => $coordinates[0],
        'lng' => $coordinates[1],
        'elevation' => $coordinates[2],
        'accuracy' => $coordinates[3],
        'privacy' => $result['add_location/privacy'] ?? $result['add_location/location_privacy'],
        'radius' => 0,
    ];

    if (!$location['lat'] || !$location['lng']) {
        $location['lat'] = $result['_geolocation'][0];
        $location['lng'] = $result['_geolocation'][1];
    }
    if (!$location['lat'] || !$location['lng']) {
        continue;
    }
    if ($location['privacy'] == 'obfuscated') {
        $location['radius'] = 13;
    }

    $features[] = [
        'type' => 'Feature',
        'properties' => [
            'name' => $name,
            'amenity' => '',
            'popupContent' => $description,
            'id' => $id,
        ],
        'geometry' => [
            'type' => 'Point',
            'coordinates' => [$location['lng'], $location['lat']]
        ]
    ];

    // releafbrain_needs
    $needData = [
        'title' => $name,
        'description' => $description,
        'message' => '',
        'category' => '',
        'priority' => 2,
        'quantity' => $result['request/quantity'],
        'quantity_unit' => 'ppl',
        'img_portrait' => '',
        'img_landscape' => '',
        'img_square' => '',
        'img_free' => $result['request/image'],
        'video' => $result['request/video'],
        'createdon' => $result['end'],
        'createdby' => '',
        'published' => 0,
        'deleted' => '',
        'kobo_response' => json_encode($result),
//        'Address_street' => '',
//        'Address_house_nr' => '',
//        'Address_subdivision' => '',
//        'Address_locality' => '',
//        'Address_region' => '',
//        'Address_country' => 'PH',
//        'Address_createdon' => '',
//        'Address_createdby' => '',
//        'Address_deleted' => 0,
//        'Location_lat' => '',
//        'Location_lng' => '',
//        'Location_elevation' => '',
//        'Location_radius' => 0,
//        'Location_geojson' => '',
//        'Location_deleted' => 0,
    ];

    // earthbrain_users
    $userData = [
        'User_username' => '',
        'User_createdon' => $result['end'],
        'User_class_key' => 'earthPerson',
        'User_active' => 0,
        'UserData_fullname' => $result['contact/name'],
        'UserData_gender' => '',
        'UserData_country' => 'PH',
        'UserData_email' => $result['add_contact/email'],
        'UserData_phone' => $result['contact/phone'],
        'UserData_website' => '',
        'ContactData_firstname' => $result['add_contact/first_name'],
        'ContactData_middlename' => $result['add_contact/middle_name'],
        'ContactData_lastname' => $result['add_contact/last_name'],
        'ContactData_phone_alt' => $result['add_contact/phone_alt'],
        'ContactData_organization' => $result['add_contact/organization'],
        'ContactData_jobtitle' => $result['add_contact/job_title'],
        'ContactData_facebook' => '',
        'ContactData_instagram' => '',
        'Address_street' => $result['add_address/street_name'],
        'Address_house_nr' => $result['add_address/house_nr'],
        'Address_subdivision' => $result['add_address/subdivision'],
        'Address_locality' => $result['add_address/locality'],
        'Address_region' => $result['add_address/region'],
        'Address_region' => '',
        'Address_country' => 'PH',
        'Address_createdon' => $result['end'],
        'Address_createdby' => '',
        'Address_deleted' => 0,
        'Location_lat' => $location['lat'],
        'Location_lng' => $location['lng'],
        'Location_elevation' => $location['elevation'],
        'Location_radius' => $location['radius'],
        'Location_geojson' => '',
        'Location_createdon' => '',
        'Location_createdby' => '',
        'Location_deleted' => 0,
    ];

//    $output[] = $modx->getChunk('releafMapNeedGeoJSON', [
//        'name' => $name,
//        'description' => $description,
//        'id' => $result['_id'],
//        'lat' => $result['_geolocation'][0],
//        'lng' => $result['_geolocation'][1],
//    ]);

    // Try to find the user who processed this request
    $createdBy = $modx->getObject('modUserProfile', ['phone' => $result['phonenumber']]);
    if (is_object($createdBy)) {
        $needData['createdby'] = $createdBy->get('internalKey');
        $userData['Address_createdby'] = $createdBy->get('internalKey');
        $userData['Location_createdby'] = $createdBy->get('internalKey');
    }

    // Create aid request, or fetch existing
    $releafNeed = $modx->getObject('releafNeed', [ 'kobo_id' => $id ]);
    if (!is_object($releafNeed)) {
        $releafNeed = $modx->newObject('releafNeed');
        $releafNeed->set('kobo_id', $id);
        $releafNeed->save();

        // Add data
        $releafNeed->fromArray($needData);

        $locationID = NULL;
        $addressID = NULL;
        $contactID = NULL;
    }
    else {
        $locationID = $releafNeed->get('location_id');
        $addressID = $releafNeed->get('address_id');
        $contactID = $releafNeed->get('contact_id');
    }

    // Create contact if it's a first request
    if ($result['contact/first_request'] === 'yes' && !$contactID) {
        $earthbrain->saveContact($releafNeed, $userData, $contactID);

        // Tie address and location to contact data
        $earthPersonData = $releafNeed->getOne('ContactData');
        $userData['classname'] = 'earthPersonData';
        $addressID = $earthPersonData->get('address_id');
        $locationID = $earthPersonData->get('location_id');

        $earthbrain->saveAddress($earthPersonData, $userData, $addressID);
        $earthbrain->saveLocation($earthPersonData, $userData, $locationID);
    }
}

//echo "<pre><code>";
//echo print_r($output, 1);
//echo "</code></pre>";

$output = [
    'type' => 'FeatureCollection',
    'features' => $features,
];

return;

return json_encode($output, JSON_PRETTY_PRINT);