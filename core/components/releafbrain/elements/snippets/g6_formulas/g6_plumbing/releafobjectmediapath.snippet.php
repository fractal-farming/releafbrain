<?php
/**
 * foodObjectMediaPath
 *
 * Based on migxObjectMediaPath. But instead of using object ID in image path,
 * you can grab a field value of choice to fill the placeholder.
 *
 * Usage example:
 * [[foodObjectMediaPath? &pathTpl=`uploads/img/forest/{fieldValue}/` &className=`foodForest` &fieldName=`resource_id`]]
 *
 * Take note that writing to the error log works slightly different inside this
 * snippet. You need to format a query with sprintf instead of writing out the
 * usual message.
 */

$pathTpl = $modx->getOption('pathTpl', $scriptProperties, '');
$objectID = $modx->getOption('objectID', $scriptProperties, '');
$className = $modx->getOption('className', $scriptProperties, '');
$fieldName = $modx->getOption('fieldName', $scriptProperties, 'resource_id');
$createFolder = $modx->getOption('createFolder', $scriptProperties, '1');
$path = '';
$createPath = false;

if (empty($objectID) && $modx->getPlaceholder('objectid')) {
    // placeholder was set by some script on frontend for example
    $objectID = $modx->getPlaceholder('objectid');
}

if (empty($objectID) && isset($_REQUEST['object_id'])) {
    $objectID = $_REQUEST['object_id'];
}

if (empty($objectID)) {
    // set session var in fields.php processor
    if (isset($_SESSION['migxWorkingObjectid'])) {
        $objectID = $_SESSION['migxWorkingObjectid'];
        $createPath = !empty($createFolder);
    }
}

// By default, fill fieldValue placeholder with object ID
$fieldValue = $objectID;

// Get desired field value from specified object
if ($className) {
    $query = $modx->newQuery($className, array(
        'id' => $objectID,
    ));
    $query->select($fieldName);
    $fieldValue = $modx->getValue($query->prepare());
}

//$modx->log(modX::LOG_LEVEL_ERROR, sprintf('[foodObjectMediaPath] Field value: %s', $fieldValue));

if (!$fieldValue) return '';

// Create path
$path = str_replace('{fieldValue}', $fieldValue, $pathTpl);
$fullPath = $modx->getOption('base_path') . $path;

if ($createPath && !file_exists($fullPath)) {
    $permissions = octdec('0' . (int)($modx->getOption('new_folder_permissions', null, '755', true)));
    if (!@mkdir($fullPath, $permissions, true)) {
        $modx->log(modX::LOG_LEVEL_ERROR, sprintf('[foodObjectMediaPath]: could not create directory %s', $fullPath));
    }
    else{
        chmod($fullPath, $permissions);
    }
}

return $path;