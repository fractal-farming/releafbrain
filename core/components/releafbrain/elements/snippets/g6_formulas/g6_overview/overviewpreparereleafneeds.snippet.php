<?php
/**
 * overviewPrepareReleafNeeds
 *
 * Modify field values before the pdoResources result is rendered.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;

//$modx->log(modX::LOG_LEVEL_ERROR, '[overviewPrepareReleafNeeds] Row: ' . print_r($row, 1));

// Images
// =============================================================================

// Render image with fixed media source path
$img = $row['img_' . $row['img_type']];
if ($img) {
    $img['class_key'] = 'releafImageNeed';
    $img['parent_id'] = $row['id'];
    $row['imageJSON'] = $earthimage->fixSourcePath($img);
}

// Parent
// =============================================================================

if ($row['parent_id']) {
    $parent = $modx->getObjectGraph('releafNeed', '{"Image":{}},{"Address":{}}', ['id'=>$row['parent_id']]);
    $siblings = $parent->getMany('Children');

    $imgType = 'img_' . $row['img_type'];
    if ($imgType == 'img_free') {
        $imgType = 'img';
    }

    $img = json_decode($parent->Image->get($imgType), true);
    if ($img) {
        $img['class_key'] = 'releafImageNeed';
        $img['parent_id'] = $row['parent_id'];
        $row['imageJSON'] = $earthimage->fixSourcePath($img);
    }

//    $output = '';
//    foreach ($siblings as $sibling) {
//        $title = $sibling->get('title');
//        if ($title == $row['title']) {
//            $title = '<strong>' . $row['title'] . '</strong>';
//        }
//        $output .= '<div>' . $title . '</div>';
//    }

    $row['subtitle'] = $row['title'];
    $row['title'] = $parent->get('title');
    $row['locality'] = $parent->Address->get('locality');
    $row['region'] = $parent->Address->get('region');
}

// Data
// =============================================================================

// Format region
if ($row['locality'] && $row['region']) {
    $row['region'] = ', ' . $row['region'];
}

return json_encode($row);