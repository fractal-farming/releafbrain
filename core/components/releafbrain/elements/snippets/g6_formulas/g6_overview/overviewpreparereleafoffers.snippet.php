<?php
/**
 * overviewPrepareComponents
 *
 * Modify field values before the pdoResources snippet in
 * overviewOuterComponents is executed.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$fbCorePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$rmCorePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$fbCorePath . 'model/earthbrain/', array('core_path' => $fbCorePath));
$romanesco = $modx->getService('romanesco','Romanesco',$rmCorePath . 'model/romanescobackyard/', array('core_path' => $rmCorePath));

if (!($earthbrain instanceof EarthBrain)) return;

//$modx->log(modX::LOG_LEVEL_ERROR, '[overviewPrepareForests] Row: ' . print_r($row, 1));
//$row = array();


// Data
// =============================================================================

$row['inputs'] = $earthbrain->getInputs($row, '', '<br>');
$row['outputs'] = $earthbrain->getOutputs($row, '', '<br>');


// Images
// =============================================================================

if ($row['image']) {
    // Generate correct image path
    $image = $earthbrain->generateImagePath($row);

    $row['imageJSON'] = json_encode($image);
}
else {
    // Provide fallback
    $row['imageFallback'] = '/uploads/img/fallback/fallback_' . $row['img_type'] . '.png';
}

return json_encode($row);