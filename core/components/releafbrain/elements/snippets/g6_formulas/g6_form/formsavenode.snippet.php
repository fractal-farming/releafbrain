<?php
/**
 * formSaveNode snippet
 *
 * Save form submissions directly to database.
 *
 * @var modX $modx
 * @var object $hook
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('releafbrain.core_path', null, $modx->getOption('core_path') . 'components/releafbrain/');
$releafbrain = $modx->getService('releafbrain','ReleafBrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($releafbrain instanceof ReleafBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$earthProcessorProps = ['processors_path' => $earthbrain->config['processorsPath']];
$releafProcessorProps = ['processors_path' => $releafbrain->config['processorsPath']];

$formID = $modx->resource->get('id');
$formPrefix = 'fb' . $formID . '-';
$formData = [];

// Remove prefix from field names and trim values
foreach ($hook->getValues() as $key => $value) {
    $key = str_replace($formPrefix,'',$key);
    if (is_string($value)) $value = trim($value);
    $formData[$key] = $value;
}

// Reuse timestamp so all dates match
$createdOn = time();

// Try to find the person who made this request
$personNew = strtolower($formData['person-new']) === 'yes';
$createdBy = $earthbrain->getPerson($formData['person-phone'], $personNew);
if (isset($createdBy['error'])) {
    $hook->addError($formPrefix.'person-phone', $createdBy['error']);
    return false;
}

// Geocode addresses
$location = $modx->runSnippet('geocodeAddress', ['address' => $formData['address'] . ', Philippines']) ?? [];
$locationPerson = [];
if ($personNew && $formData['person-address']) {
    $locationPerson = $modx->runSnippet('geocodeAddress', ['address' => $formData['person-address']]);
}

// Data arrays
$nodeData = [
    'network_id' => $formData['network_id'],
    'title' => $formData['title'],
    'description' => $formData['description'],
    'type' => $formData['type'],
    'images' => str_replace(',', ' ', $formData['images']),
    'links' => $formData['links'],
    'networks' => strtolower($formData['networks']) === 'yes',
    'network-details' => $formData['network-details'],
    'createdon' => $createdOn,
    'createdby' => $createdBy,
    'published' => 0,
];
$nodeAddress = [
    'line_1' => trim($location['properties']['streetNumber'] . ' ' . $location['properties']['streetName']),
    'line_2' => '',
    'line_3' => $location['properties']['subLocality'] ?? '',
    'locality' => $location['properties']['locality'] ?? '',
    'region' => $location['properties']['adminLevels'][1]['name'] ?? '',
    'country' => $location['properties']['countryCode'] ?? '',
    'postal_code' => $location['properties']['postalCode'] ?? '',
    'comments' => $formData['address'],
    'createdon' => $createdOn,
    'createdby' => $createdBy,
    'published' => 1,
];
$nodeLocation = [
    'lat' => $location['geometry']['coordinates'][1],
    'lng' => $location['geometry']['coordinates'][0],
    'elevation' => null,
    'radius' => '',
    'geojson' => null,
    'createdon' => $createdOn,
    'createdby' => $createdBy,
    'published' => 1,
];

$personData = [
    'firstname' => '',
    'middlename' => '',
    'lastname' => '',
    'fullname' => $formData['person-name'],
    'address' => $formData['person-address'],
    'country' => $locationPerson['properties']['countryCode'],
    'email' => $formData['person-email'],
    'phone' => $formData['person-phone'],
    'website' => '',
    'createdon' => $createdOn,
    'class_key' => 'earthPerson',
    'active' => 0,
    'new' => $personNew,
    'privacy' => $formData['person-privacy'],
    'newsletter' => $formData['newsletter'],
];
$personAddress = [
    'line_1' => trim($locationPerson['properties']['streetNumber'] . ' ' . $locationPerson['properties']['streetName']),
    'line_2' => '',
    'line_3' => $locationPerson['properties']['subLocality'] ?? '',
    'locality' => $locationPerson['properties']['locality'] ?? '',
    'region' => $locationPerson['properties']['adminLevels'][1]['name'] ?? '',
    'country' => $locationPerson['properties']['countryCode'] ?? '',
    'postal_code' => $locationPerson['properties']['postalCode'] ?? '',
    'comments' => $formData['person-address'],
    'createdon' => $createdOn,
    'createdby' => '',
    'published' => 0,
];
$personLocation = [
    'lat' => $locationPerson['geometry']['coordinates'][1],
    'lng' => $locationPerson['geometry']['coordinates'][0],
    'elevation' => null,
    'radius' => '',
    'geojson' => null,
    'createdon' => $createdOn,
    'createdby' => '',
    'published' => 0,
];

// Create single array for processor
$nodeData['nodeAddress'] = $nodeAddress;
$nodeData['nodeLocation'] = $nodeLocation;
$nodeData['personData'] = $personData;
$nodeData['personAddress'] = $personAddress;
$nodeData['personLocation'] = $personLocation;

// Create releaf request
$response = $modx->runProcessor('data/node/create', $nodeData, $releafProcessorProps);
if ($response->isError()) {
    $modx->log(modX::LOG_LEVEL_ERROR, $response->getAllErrors());
    return false;
}

return true;