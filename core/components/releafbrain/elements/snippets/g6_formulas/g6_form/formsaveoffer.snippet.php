<?php
/**
 * formSaveOffer snippet
 *
 * WIP. Save form submissions directly to database.
 *
 * @var modX $modx
 * @var object $hook
 * @var array $scriptProperties
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Geocoder\Provider\Mapbox\Mapbox;
use Geocoder\Provider\LocationIQ\LocationIQ;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\Exception\Exception;
use Geocoder\Dumper\GeoArray;

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','earthbrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('releafbrain.core_path', null, $modx->getOption('core_path') . 'components/releafbrain/');
$releafbrain = $modx->getService('releafbrain','releafbrain',$corePath . 'model/releafbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($releafbrain instanceof ReleafBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$config = [
    'timeout' => 5.0,
    'verify' => true,
];
$httpClient = new GuzzleHttp\Client($config);
$geocoder = new LocationIQ($httpClient, $modx->getOption('earthbrain.locationiq_access_token', 'eu1'));
//$geocoder = new Mapbox($httpClient, $modx->getOption('earthbrain.mapbox_access_token'));

// Collect input from form fields
$formData = $hook->getValues();
$formID = $modx->resource->get('id');
$prefix = 'fb' . $formID . '-';

//$modx->log(modX::LOG_LEVEL_ERROR, $accessToken);
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($formData,1));
//return false;

$categories = $formData[$prefix.'category'];
$createdOn = time();

// Geocode address
$address = $formData[$prefix.'person-address'];
if ($address) {
    $address = str_replace("\r\n", ', ', $address) . ', Philippines';
    $location = [];

    // Cache results, to prevent unnecessary API requests
    $cacheManager = $modx->getCacheManager();
    $cacheKey = 'geocoder';
    $cacheElementKey = 'locations/' . md5(json_encode($address));
    $cacheLifetime = 86400 * 365;
    $cacheOptions = [
        xPDO::OPT_CACHE_KEY => $cacheKey,
        xPDO::OPT_CACHE_EXPIRES => $cacheLifetime,
    ];

    // Check the cache first
    $locationCached = $cacheManager->get($cacheElementKey, $cacheOptions);

    // If a cached result was found, use that data
    if ($locationCached) {
        $location = $locationCached;
    } else {
        try {
            $dumper = new GeoArray();
            $location = $geocoder->geocodeQuery(GeocodeQuery::create($address));
            $location = $dumper->dump($location->first());
        } catch (Exception $e) {
            $modx->log(modX::LOG_LEVEL_ERROR, 'Oh jee.. [' . $e->getCode() . '] ' . $e->getMessage());
        }

        // Cache result
        $cacheManager->set($cacheElementKey, $location, $cacheLifetime, $cacheOptions);
    }
}
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($location,1));
//return false;

// Reformat phone number
$personPhone = $formData[$prefix.'person-phone'];
$personPhone = strip_tags($personPhone); // strip HTML
$personPhone = str_replace('+63', '0', $personPhone); // replace country prefix
$personPhone = preg_replace('/[^0-9]/', '', $personPhone); // strip non-numeric characters
$personPhone = preg_replace('/\s+/', '', $personPhone); // strip whitespace

// Respect privacy
$privacyName = 0;
$privacyLocation = 0;
if (str_contains($formData[$prefix . 'person-privacy'], 'name')) {
    $privacyName = 1;
}
if (str_contains($formData[$prefix . 'person-privacy'], 'location')) {
    $privacyLocation = 1;
}

$offerData = [
    'title' => $formData[$prefix.'title'],
    'description' => $formData[$prefix.'description'],
    'message' => $formData[$prefix.'description'],
    'category' => $formData[$prefix.'category'],
    'tags' => $formData[$prefix.'tags'],
    'quantity' => $formData[$prefix.'quantity'],
    'quantity_unit' => $formData[$prefix.'quantity-unit'],
    'stock' => $formData[$prefix.'quantity'],
    'createdon' => $createdOn,
    'createdby' => '',
    'published' => 0,
];

$personData = [
    'UserData_fullname' => $formData[$prefix.'person-name'],
    'UserData_address' => $formData[$prefix.'person-address'],
    'UserData_country' => 'PH',
    'UserData_email' => $formData[$prefix.'person-email'],
    'UserData_phone' => $personPhone,
    'UserData_website' => '',
    'Person_createdon' => $createdOn,
    'Person_class_key' => 'earthPerson',
    'Person_active' => 0,
    'PersonData_firstname' => '',
    'PersonData_middlename' => '',
    'PersonData_lastname' => '',
    'PersonData_published' => $privacyName,
    'Address_line_1' => $location['properties']['streetNumber'] . ' ' . $location['properties']['streetName'],
    'Address_line_2' => '',
    'Address_line_3' => $location['properties']['subLocality'],
    'Address_locality' => $location['properties']['locality'],
    'Address_region' => $location['properties']['adminLevels'][1]['name'],
    'Address_country' => 'PH',
    'Address_postal_code' => $location['properties']['postalCode'],
    'Address_comments' => $formData[$prefix.'address'],
    'Address_createdon' => '',
    'Address_createdby' => '',
    'Address_published' => $privacyLocation,
    'Location_lat' => $location['geometry']['coordinates'][1],
    'Location_lng' => $location['geometry']['coordinates'][0],
    'Location_elevation' => '',
    'Location_radius' => '',
    'Location_geojson' => '',
    'Location_createdon' => '',
    'Location_createdby' => '',
    'Location_published' => $privacyLocation,
];

// Try to find the user who processed this request
$personID = '';
$createdBy = $modx->getObject('modUserProfile', ['phone' => $personPhone]);

// Abort if existing user can't be matched by phone number
if (strtolower($formData[$prefix.'person-new']) === 'no' && !is_object($createdBy)) {
    $hook->addError($prefix.'person-phone', "We couldn't find this phone number in our database.");
    return false;
}

// Reversely, abort if new user entered an existing phone number
if (strtolower($formData[$prefix.'person-new']) === 'yes' && is_object($createdBy)) {
    $hook->addError($prefix.'person-phone', "This phone number already exists in our database! Are you sure this is your first submission?");
    return false;
}

// Get existing user ID
if (is_object($createdBy)) {
    $personID = $createdBy->get('internalKey');

    $offerData['person_id'] = $personID;
    $offerData['createdby'] = $personID;
    $personData['createdby'] = $personID;
    $personData['Address_createdby'] = $personID;
    $personData['Location_createdby'] = $personID;
}

// Create offer
$releafOffer = $modx->newObject('releafOffer');
$releafOffer->fromArray($offerData);
$releafOffer->save();

// Create earthPerson if it's a new donor
if (strtolower($formData[$prefix.'person-new']) === 'yes' && !$personID) {
    $result = $earthbrain->savePerson($releafOffer, $personData, null);

    // Get new user ID
    $personID = $result['id'];

    // Use createdby field to reference donor
    $releafOffer->set('person_id', $personID);
    $releafOffer->set('createdby', $personID);
    $releafOffer->save();

    // Tie address and location to contact data
    $earthPersonData = $releafOffer->getOne('PersonData');
    $personData['classname'] = 'earthPersonData';
    $addressID = $earthPersonData->get('address_id');
    $locationID = $earthPersonData->get('location_id');

    $earthbrain->saveAddress($earthPersonData, $personData, $addressID);
    $earthbrain->saveLocation($earthPersonData, $personData, $locationID);
}

return true;