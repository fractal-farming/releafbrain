<?php
/**
 * migxSaveReleafNeed
 *
 * Aftersave snippet for Need requests.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','earthbrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$co_id = $modx->getOption('co_id', $properties);

$objectID = null;
$parentID = null;
$addressID = null;
$locationID = null;

if (is_object($object)) {
    $objectID = $object->get('object_id');
    $parentID = $object->get('parent_id');
    $addressID = $object->get('address_id');
    $locationID = $object->get('location_id');

    // Safeguard against selecting itself as parent
    if ($objectID == $parentID) {
        return json_encode(['error' => 'You can\'t select this item as its own parent!']);
    }

    // Link nested item to parent
    if ($objectID == 'new' && $co_id && !$parentID) {
        $object->set('parent_id', $co_id);
        $object->save();
    }
}

$earthbrain->resetNull($object, $properties);
$earthbrain->saveAddress($object, $properties, $addressID);
$earthbrain->saveLocation($object, $properties, $locationID);

return '';