<?php
/**
 * ResourceSaveOrganization
 *
 * Check if this resource already has a row in the releafbrain_organizations
 * table and create one if that's not the case.
 *
 * @var modX $modx
 * @var modResource $resource
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('releafbrain.core_path', null, $modx->getOption('core_path') . 'components/releafbrain/');
$releafbrain = $modx->getService('releafbrain','ReleafBrain', $corePath . 'model/releafbrain/', array('core_path' => $corePath));

if (!($releafbrain instanceof ReleafBrain)) return;
if (!($resource instanceof modResource)) return;

$templateID = $resource->get('template');

// Abort if resource template is not ReleafOrganization
if ($templateID != $modx->getOption('releafbrain.org_template_id')) {
    return;
}

switch ($modx->event->name) {
    case 'OnDocFormRender':
        /**
         * @var int $id
         * @var string $mode
         */

        // Don't bother with new pages (because there is no ID yet)
        if ($mode === 'new') return true;

        // Try to get data set for this resource
        $data = $modx->getObject('releafOrganization', array('resource_id' => $id));

        // Create new entry for this resource
        if (!is_object($data)) {
            $data = $modx->newObject('releafOrganization', array(
                'resource_id' => $id,
                'createdon' => time(),
                'createdby' => $modx->user->get('id'),
            ));
            $data->save();
        }

        break;
}