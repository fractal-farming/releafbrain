[[$imgOverviewReleafFallback?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<div class="center aligned content title">
    <div class="header">[[+name]]</div>
    <p class="meta">[[+quantity:append=` [[+quantity_unit]]`]]</p>
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<p class="meta">[[+locality:append=`[[+region:notempty=`, [[+region]]`]]`]]</p>`
    ]]
    [[If?
        &subject=`[[+show_introtext]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="description">[[+description]]</div>`
    ]]
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview? &classes=`large primary bottom attached``
]]]]