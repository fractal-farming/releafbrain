<td data-title="[[%earthbrain.component.heading]]">
    [[+img_landscape:notempty=`
    <figure class="ui image">
        <img class="ui mini image" src="[[ImagePlus? &value=`[[+imageJSON]]` &options=`w=200&zc=1` &type=`thumb`]]" alt="[[+pagetitle]]">
    </figure>
    `]]
    [[+pagetitle]]
</td>
<td data-title="[[%earthbrain.component.inputs_heading]]">
    [[+inputs]]
</td>
<td data-title="[[%earthbrain.component.outputs_heading]]">
    [[+outputs]]
</td>
<td data-title="[[%earthbrain.component.data_stage]]">
    [[+stage:ucfirst]]
</td>
<td data-title="[[%earthbrain.component.data_zone]]">
    [[+zone]]
</td>
[[-
<td data-title="[[%earthbrain.component.maintainer]]">
    [[+maintainer]]
</td>
<td>
    <a href="[[~[[+id]]]]" class="ui compact primary button">
        [[%earthbrain.map.button_read_more]]
    </a>
</td>
]]