<div class="ui nested stackable doubling relaxed divided equal width grid">
    <div class="row">
        <div class="six wide column">
            [[$imgOverviewReleafFigure?
                &uid=`[[+unique_idx]]`
                &classes=``
            ]]

            <a href="" class="large primary fluid ui button">[[%releafbrain.offer.donate]]</a>
        </div>
        <div class="column">
            [[$headingOverview?
                &title_field=`title`
                &uid=`[[+unique_idx]]`
            ]]
            [[If?
                &subject=`[[+show_subtitle]]`
                &operator=`EQ`
                &operand=`1`
                &then=`<p class="meta subtitle"><i class="marker icon"></i>[[+locality]][[+region]]</p>`
            ]]
            [[If?
                &subject=`[[+show_introtext]]`
                &operator=`EQ`
                &operand=`1`
                &then=`<div class="description">[[+description]]</div>`
            ]]

            [[migxLoopCollection
                :prepend=`<h4 class="ui header">[[%releafbrain.need.community_needs]]</h4>`
                :empty=`<a class="large secondary right floated ui button" href="[[~[[++releafbrain.offer_pledge_id]]? &need=`[[+id]]`]]">[[%releafbrain.need.respond]]</a>`
                ?
                &packageName=`releafbrain`
                &classname=`releafNeed`
                &where=`[{"parent_id":"[[+id]]"},{"deleted:=":0}]`
                &tpl=`releafNeedTableBasicRow`
                &wrapperTpl=`releafNeedTableBasicWrapper`
                &outputSeparator=``
                &sortConfig=`[{"sortby":"createdon","sortdir":"ASC"}]`
            ]]
        </div>
    </div>
</div>
