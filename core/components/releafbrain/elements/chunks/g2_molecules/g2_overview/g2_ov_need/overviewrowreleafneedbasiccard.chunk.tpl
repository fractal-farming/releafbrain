[[$imgOverviewReleafFigure?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<div class="center aligned content title">
    <div class="header">[[+title]]</div>
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<p class="meta">[[+locality]][[+region]]</p>`
    ]]

    [[If?
        &subject=`[[+show_introtext]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="description">[[+description]]</div>`
    ]]
</div>

[[$releafNeedButtonRespond?
    &classes=`large bottom attached`
    &uid=`[[+unique_idx]]`
]]