[[-$imgOverviewReleafLink?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<i class="large middle aligned map marker alternate icon disabled"></i>

<div class="content">
    [[$headingOverview?
        &title_field=`title`
        &uid=`[[+unique_idx]]`
    ]]
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="meta">[[+locality]][[+region]]</div>`
    ]]
    [[If?
        &subject=`[[+show_introtext]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="description">[[+description]]</div>`
    ]]
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview? &classes=```
]]]]