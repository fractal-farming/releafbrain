[[$imgOverviewReleafFigure?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

[[[[If?
    &subject=`[[+show_subtitle]]`
    &operator=`EQ`
    &operand=`1`
    &then=`$headingOverviewSubtitle? &title_field=`title` &uid=`[[+unique_idx]]``
    &else=`$headingOverview? &title_field=`title` &uid=`[[+unique_idx]]``
]]]]

[[[[If?
    &subject=`[[+show_introtext]]`
    &operator=`EQ`
    &operand=`1`
    &then=`$introtextDescription? &uid=`[[+unique_idx]]``
]]]]

<div class="ui middle aligned list">
    <div class="item">[[+locality]][[+region]]</div>
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview:prepend=`<p>`:append=`</p>`? &classes=`large``
]]]]