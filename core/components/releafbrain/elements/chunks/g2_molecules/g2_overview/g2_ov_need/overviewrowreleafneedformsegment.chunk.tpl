<div class="ui nested stackable equal width grid">
    <div class="row">
        <div class="five wide column">
            [[$imgOverviewReleafFigure?
                &uid=`[[+unique_idx]]`
                &classes=`rounded`
            ]]
        </div>
        <div class="column">
            [[$headingOverview?
                &title_field=`title`
                &uid=`[[+unique_idx]]`
            ]]
            [[If?
                &subject=`[[+subtitle]]`
                &operator=`notempty`
                &then=`<p class="meta subtitle">[[+subtitle]]</p>`
            ]]
            [[If?
                &subject=`[[+show_subtitle]]`
                &operator=`EQ`
                &operand=`1`
                &then=`<p class="meta subtitle"><i class="marker icon"></i>[[+locality]][[+region]]</p>`
            ]]
        </div>
    </div>
</div>
