<td data-title="[[%releafbrain.need.heading]]">
    [[+title]]<br>
    <em class="meta"><i class="small map marker alternate icon"></i> [[+locality]][[+region]]</em>
</td>

<td data-title="[[%releafbrain.need.community_size]]">
    [[+community_size:prepend=`<i class="small disabled user icon"></i> `]]
</td>

<td data-title="[[%releafbrain.need.category]]">
    [[+category:ucfirst]]
</td>

<td data-title="[[%releafbrain.need.organization]]">
    [[#[[+organization_id]].pagetitle]]
</td>

<td>
    [[$releafNeedButtonRespond?
        &classes=`compact primary`
        &uid=`[[+unique_idx]]`
    ]]
</td>