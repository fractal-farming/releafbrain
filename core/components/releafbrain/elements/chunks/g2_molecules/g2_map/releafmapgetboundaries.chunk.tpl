[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/maps``]]

    &parents=`[[+parents:default=`-1`]]`
    &resources=`[[+forest_id]]`
    &context=`[[+context:default=`forestbrain`]]`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`@INLINE [[+geojson]]`
    &tplWrapper=`foodMapWrapperGeoJSON`
    &outputSeparator=`,`
    &where=`[
        {"template:IN":[100001,100004]},
        {"Location.geojson:!=":""},
        {"published:=":1}
    ]`
    &showHidden=`1`

    &leftJoin=`{
        "Data": {
            "class": "foodForest",
            "on": "modResource.id = Data.resource_id"
        },
        "Location": {
            "class": "foodLocation",
            "on": "Data.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "modResource.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`modResource.id`
    &select=`{
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.elevation AS elevation, Location.geojson AS geojson",
        "CreatedBy": "CreatedBy.fullname AS maintainer",
        "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
    }`

    &prepareSnippet=`foodMapPrepareFeatures`

    &showLog=`0`
]]