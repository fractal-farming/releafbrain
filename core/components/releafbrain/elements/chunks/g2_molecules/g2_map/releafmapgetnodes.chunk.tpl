[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/nodes/[[+user_access_level]]``]]
    &class=`releafNode`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`releafMapNeedGeoJSON`
    &tplWrapper=`releafMapWrapperGeoJSON`
    &where=`[
        {"Location.lat:!=":""},
        {"Location.lng:!=":""}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Address": {
            "class": "earthAddress",
            "on": "releafNode.address_id = Address.id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "releafNode.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "releafNode.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`releafNode.id`
    &select=`{
        "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
        "CreatedBy": "CreatedBy.fullname AS mapper",
        "Request": "releafNode.id AS id, releafNode.title AS name, releafNode.description AS description"
    }`

    &showLog=`0`
]]