[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/needs/[[+user_access_level]]``]]
    &class=`releafNeed`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`releafMapNeedGeoJSON`
    &tplWrapper=`releafMapWrapperGeoJSON`
    &where=`[
        {"Location.lat:!=":""},
        {"Location.lng:!=":""}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Address": {
            "class": "earthAddress",
            "on": "releafNeed.address_id = Address.id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "releafNeed.location_id = Location.id"
        },
        "Image": {
            "class": "earthImage",
            "on": "releafNeed.image_id = Image.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "releafNeed.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`releafNeed.id`
    &select=`{
        "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
        "Image": "Image.img AS img_free, Image.img_landscape AS img_landscape, Image.img_portrait AS img_portrait, Image.img_square AS img_square, Image.img_wide AS img_wide, Image.img_pano AS img_pano",
        "CreatedBy": "CreatedBy.fullname AS mapper",
        "Need": "releafNeed.id AS id, releafNeed.title AS title, releafNeed.description AS description"
    }`
    &showLog=`0`

    &prepareSnippet=`releafMapPrepareNeeds`
    &tplPopupContent=`releafMapNeedPopupContent`
]]