[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/offers/[[+user_access_level]]``]]
    &class=`releafOffer`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`releafMapOfferGeoJSON`
    &tplWrapper=`releafMapWrapperGeoJSON`
    &where=`[
        {"Location.lat:!=":""},
        {"Location.lng:!=":""}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Person": {
            "class": "modUserProfile",
            "on": "releafOffer.person_id = Person.internalKey"
        },
        "PersonData": {
            "class": "earthPersonData",
            "on": "Person.id = PersonData.person_id"
        },
        "Address": {
            "class": "earthAddress",
            "on": "PersonData.address_id = Address.id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "PersonData.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "releafOffer.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`releafOffer.id`
    &select=`{
        "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
        "CreatedBy": "CreatedBy.fullname AS giver",
        "Offer": "releafOffer.id AS id, releafOffer.title AS title, releafOffer.description AS description, releafOffer.quantity AS quantity, releafOffer.quantity_unit AS quantity_unit"
    }`

    &showLog=`0`
]]