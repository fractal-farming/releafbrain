[[+layout_title:stripAsAlias:empty=`ro`:toPlaceholder=`ov_id`]]
[[setBoxType? &input=`[[+row_tpl]]` &prefix=`[[+ov_id]]_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`
    &c1=`menuindex_0`     &do1=`ASC`
    &c2=`publishedon_0`   &do2=`DESC`
    &c3=`createdon_0`     &do3=`DESC`
    &c4=`pagetitle_0`     &do4=`ASC`

    &c5=`menuindex_1`     &do5=`DESC`
    &c6=`publishedon_1`   &do6=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`pagetitle_1`     &do8=`DESC`

    &default=`DESC`
]]

<table id="[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`rn_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/offers/[[+user_access_level]]``]]
        &class=`releafOffer`

        &depth=`0`
        &limit=`[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`[[+cols:textToNumber:mpy=`2`]]` &else=`[[+limit:default=`0`]]`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowReleafOffer[[+[[+prefix]].row_type]]`
        &sortby=`id`

        &leftJoin=`{
            "Address": {
                "class": "earthAddress",
                "on": "releafOffer.address_id = Address.id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "releafOffer.location_id = Location.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "releafOffer.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`releafOffer.id`
        &select=`{
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "CreatedBy": "CreatedBy.fullname AS giver",
            "Offer": "releafOffer.id AS id, releafOffer.title AS title, releafOffer.quantity AS quantity, releafOffer.quantity_unit AS quantity_unit"
        }`

        [[-&prepareSnippet=`overviewPrepareReleafOffers`]]

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
