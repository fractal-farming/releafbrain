[[+layout_title:stripAsAlias:empty=`rn`:toPlaceholder=`ov_id`]]
[[setBoxType? &input=`[[+row_tpl]]` &prefix=`[[+ov_id]]_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`
    &c1=`menuindex_0`     &do1=`ASC`
    &c2=`publishedon_0`   &do2=`DESC`
    &c3=`createdon_0`     &do3=`DESC`
    &c4=`pagetitle_0`     &do4=`ASC`

    &c5=`menuindex_1`     &do5=`DESC`
    &c6=`publishedon_1`   &do6=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`pagetitle_1`     &do8=`DESC`

    &default=`DESC`
]]

<table id="[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`rn_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head?
        &db_fields=`[[+[[+prefix]].dataset_content]]`
    ]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/needs/[[+user_access_level]]``]]
        &class=`releafNeed`

        &depth=`0`
        &limit=`[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`[[+cols:textToNumber:mpy=`2`]]` &else=`[[+limit:default=`0`]]`]]`
        &offset=`0`
        &tpl=`overviewRowReleafNeed[[+[[+prefix]].row_type]]`
        &sortby=`id`
        &where=`{"parent_id:=":0}`

        &leftJoin=`{
            "Address": {
                "class": "earthAddress",
                "on": "releafNeed.address_id = Address.id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "releafNeed.location_id = Location.id"
            },
            "Image": {
                "class": "earthImage",
                "on": "releafNeed.image_id = Image.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "releafNeed.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`releafNeed.id`
        &select=`{
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "Image": "Image.img AS img_free, Image.img_landscape AS img_landscape, Image.img_portrait AS img_portrait, Image.img_square AS img_square, Image.img_wide AS img_wide, Image.img_pano AS img_pano",
            "CreatedBy": "CreatedBy.fullname AS mapper",
            "Need": "releafNeed.id AS id, releafNeed.organization_id AS organization_id, releafNeed.title AS title, releafNeed.description AS description, releafNeed.category AS category, releafNeed.community_size AS community_size"
        }`

        &prepareSnippet=`overviewPrepareReleafNeeds`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
