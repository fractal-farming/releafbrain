[[+layout_title:stripAsAlias:empty=`ro`:toPlaceholder=`ov_id`]]
[[setBoxType? &input=`[[+row_tpl]]` &prefix=`[[+ov_id]]_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`
    &c1=`menuindex_0`     &do1=`ASC`
    &c2=`publishedon_0`   &do2=`DESC`
    &c3=`createdon_0`     &do3=`DESC`
    &c4=`pagetitle_0`     &do4=`ASC`

    &c5=`menuindex_1`     &do5=`DESC`
    &c6=`publishedon_1`   &do6=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`pagetitle_1`     &do8=`DESC`

    &default=`DESC`
]]

[[+pagination:eq=`1`:then=`
<div id="[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`ro_[[Time]]` &else=`[[+prefix]]`]]" class="pagination-wrapper">
`]]
    <div class="ui [[+cols]] [[+[[+prefix]].grid_settings]] [[+responsive:replace=`,== `]] [[+padding]] nested overview [[+[[+prefix]].box_type]]">
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/offers/[[+user_access_level]]``]]
        &class=`releafOffer`

        &depth=`0`
        &limit=`[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`[[+cols:textToNumber:mpy=`2`]]` &else=`[[+limit:default=`0`]]`]]`
        &offset=`0`
        &tpl=`overviewRowReleafOffer[[+[[+prefix]].row_type]]`
        &sortby=`id`

        &leftJoin=`{
            "Address": {
                "class": "earthAddress",
                "on": "releafOffer.address_id = Address.id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "releafOffer.location_id = Location.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "releafOffer.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`releafOffer.id`
        &select=`{
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "CreatedBy": "CreatedBy.fullname AS giver",
            "Offer": "releafOffer.id AS id, releafOffer.title AS title, releafOffer.quantity AS quantity, releafOffer.quantity_unit AS quantity_unit"
        }`

        &row_tpl=`[[If? &subject=`[[$[[+row_tpl]]Theme]]` &operator=`isnull` &then=`[[+row_tpl]]` &else=`[[+row_tpl]]Theme`]]`
        &box_type=`[[+[[+prefix]].box_type]]`
        &row_type=`[[+[[+prefix]].row_type]]`
        &column_type=`[[+[[+prefix]].column_type]]`
        &grid_settings=`[[+[[+prefix]].grid_settings]]`
        &cols=`[[+cols:textToNumber]]`
        &padding=`[[+padding]]`
        &alignment=`[[+alignment]]`
        &responsive=`[[+responsive]]`
        &prefix=`[[+prefix]]`
        &unique_idx=`[[+unique_idx]]`
        &title_field=`[[+title_field]]`
        &title_hierarchy=`[[+title_hierarchy]]`
        &classes=`[[+title_hierarchy:is=`span`:then=`tiny`]]`
        &show_subtitle=`[[+show_subtitle]]`
        &show_introtext=`[[+show_introtext]]`
        &link_text=`[[If? &subject=`[[+link_text]]` &operator=`isnull` &then=`0` &else=`[[+link_text]]`]]`
        &de_emphasize=`[[+de_emphasize]]`
        &img_type=`[[+img_type]]`
        &icon_type=`[[+icon_type]]`
        &lazy_load=`[[+lazy_load]]`

        [[+pagination:eq=`1`:then=`
        &ajaxMode=`[[+pagination_type]]`
        &ajaxElemWrapper=`#[[+prefix]]`
        &ajaxElemRows=`#[[+prefix]] .overview`
        &ajaxElemPagination=`#[[+prefix]] .pagination`
        &ajaxElemLink=`#[[+prefix]] .pagination a`
        &ajaxElemMore=`#[[+prefix]] .more`
        &scrollTop=`0`

        &tplPageWrapper=`pageNavWrapper`
        &tplPage=`pageNavItem`
        &tplPageActive=`pageNavItemActive`
        &tplPageFirst=`pageNavItemIcon@PaginationFirst`
        &tplPageLast=`pageNavItemIcon@PaginationLast`
        &tplPagePrev=`pageNavItemIcon@PaginationPrev`
        &tplPageNext=`pageNavItemIcon@PaginationNext`
        &tplPageSkip=`pageNavItemDisabled`
        &tplPageFirstEmpty=`pageNavItemIconDisabled@PaginationFirst`
        &tplPageLastEmpty=`pageNavItemIconDisabled@PaginationLast`
        &tplPagePrevEmpty=`pageNavItemIconDisabled@PaginationPrev`
        &tplPageNextEmpty=`pageNavItemIconDisabled@PaginationNext`
        &ajaxTplMore=`pageNavItemLoadMore`

        &pageVarKey=`[[+prefix]]-page`
        &pageNavVar=`[[+prefix]].page.nav`
        `]]
    ]]
    </div>
[[[[+pagination:eq=`1`:then=`$paginationFluid:append=`
</div>
`? &prefix=`[[+prefix]]``]]]]
