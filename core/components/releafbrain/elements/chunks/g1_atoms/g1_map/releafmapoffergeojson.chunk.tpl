{
    "type": "Feature",
    "properties": {
        "name": "[[+title]]",
        "amenity": "",
        "popupContent": "[[-+description:stripForJS]]",
        "id": "offer-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}