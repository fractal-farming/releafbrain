{
    "type": "Feature",
    "properties": {
        "name": "[[+name]]",
        "amenity": "",
        "popupContent": [[+popup_content:default=`""`]],
        "id": "need-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}