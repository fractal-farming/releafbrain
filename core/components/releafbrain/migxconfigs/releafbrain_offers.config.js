{
    "formtabs": [
        {
            "MIGX_id": 1,
            "caption": "Offer",
            "print_before_tabs": "0",
            "pos": "",
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "title",
                    "caption": "Title",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "description",
                    "caption": "Description",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "category",
                    "caption": "Category",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@ReleafCategory",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "tags",
                    "caption": "Tags",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox-multiple",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@ReleafTags",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "quantity",
                    "caption": "Quantity",
                    "description": "Amount",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "quantity_unit",
                    "caption": "Quantity",
                    "description": "Unit of measure",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectInputOption@UnitOfMeasure",
                    "default": "g",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "stock",
                    "caption": "Stock",
                    "description": "Number of items remaining",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdon",
                    "caption": "Date",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "createdby",
                    "caption": "Steward",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "published",
                    "caption": "Public",
                    "description": "When set to 'Yes', this item will be visible to the outside world. When set to 'No', only registered members can view this item.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "Yes==1||No==",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 2,
            "caption": "Contact",
            "description": "",
            "print_before_tabs": "0",
            "pos": "",
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "contact_id",
                    "caption": "Contact",
                    "description": "Select a registered ReleafBrain member.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "superboxselect",
                    "validation": "",
                    "configs": {
                        "selectType": "users",
                        "maxElements": "1",
                        "allowBlank": "1",
                        "pageSize": "20"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "location_alt",
                    "caption": "Pick up location",
                    "description": "If this item is stored in a different location, please provide the details in the adjacent Address and Location tabs.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "option",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "Same as contact address==||Alternative address==1",
                    "default": "",
                    "useDefaultIfEmpty": "1",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 3,
            "caption": "Address",
            "print_before_tabs": "0",
            "pos": "",
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "Address_id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_line_1",
                    "caption": "Line 1",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_line_2",
                    "caption": "Line 2",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_line_3",
                    "caption": "Line 3",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_locality",
                    "caption": "Locality",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_region",
                    "caption": "Region",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_country",
                    "caption": "Country",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "true",
                        "typeAheadDelay": "400"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectCountry",
                    "default": "PH",
                    "useDefaultIfEmpty": 1,
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_postal_code",
                    "caption": "Postal code",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "text",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_comments",
                    "caption": "Comments",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "richtext",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_createdon",
                    "caption": "Date",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "date",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_createdby",
                    "caption": "Steward",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Address_deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 5,
            "caption": "Location",
            "print_before_tabs": "0",
            "pos": "",
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "Location_id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_picker",
                    "caption": "Choose location",
                    "description": "EXPERIMENTAL. Hopefully one day, a location picker will appear here.",
                    "description_is_code": "0",
                    "inputTV": "geo_tv",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_lat",
                    "caption": "Latitude",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "minValue": "-90",
                        "maxValue": "90",
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "6",
                        "decimalSeparator": "."
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_lng",
                    "caption": "Longitude",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "minValue": "-180",
                        "maxValue": "180",
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "6",
                        "decimalSeparator": "."
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_elevation",
                    "caption": "Elevation",
                    "description": "Land height above sea level, in meters.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": {
                        "allowDecimals": "Yes",
                        "allowNegative": "Yes",
                        "decimalPrecision": "2",
                        "decimalSeparator": ","
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_geojson",
                    "caption": "Features",
                    "description": "A JSON object for displaying slightly more complex spatial data, such as a polygon for defining an area or a circle for specifying the radius of a tree. You can group multiple GeoJSON features together in a FeatureCollection, allowing you to add a variety of objects. To make it easier to create and edit this data, you can copy\/paste the code in an online editor like https:\/\/geojson.io\/.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "textarea",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "Location_deleted",
                    "caption": "deleted",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 8,
            "caption": "Images",
            "print_before_tabs": "0",
            "pos": 8,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "images",
                    "caption": "Images",
                    "description": "Attach one or more images to this item. You can sort images by dragging them up or down.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "earthbrain_images:earthbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        },
        {
            "MIGX_id": 13,
            "caption": "Notes",
            "print_before_tabs": "0",
            "pos": 13,
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "notes",
                    "caption": "Notes",
                    "description": "Anything else you'd like to add?",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "migxdb",
                    "validation": "",
                    "configs": "earthbrain_notes:earthbrain",
                    "restrictive_condition": "[[If? &subject=`[[+createdon]]` &operator=`empty` &then=`break`]]",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        }
    ],
    "contextmenus": "update||duplicate||recall_remove_delete",
    "actionbuttons": "addItem||toggletrash",
    "columnbuttons": "",
    "extended": {
        "migx_add": "Add Offer",
        "disable_add_item": "",
        "add_items_directly": "",
        "formcaption": "",
        "update_win_title": "Edit Offer",
        "win_id": "releaf_offer",
        "maxRecords": "",
        "addNewItemAt": "top",
        "media_source_id": "",
        "multiple_formtabs": "",
        "multiple_formtabs_label": "",
        "multiple_formtabs_field": "",
        "multiple_formtabs_optionstext": "",
        "multiple_formtabs_optionsvalue": "",
        "actionbuttonsperrow": 4,
        "winbuttonslist": "",
        "extrahandlers": "this.handleColumnSwitch",
        "filtersperrow": 4,
        "packageName": "releafbrain",
        "classname": "releafOffer",
        "task": "",
        "getlistsort": "id",
        "getlistsortdir": "DESC",
        "sortconfig": "",
        "gridpagesize": 30,
        "use_custom_prefix": "0",
        "prefix": "",
        "grid": "",
        "gridload_mode": 2,
        "check_resid": 0,
        "check_resid_TV": "",
        "join_alias": "",
        "has_jointable": "yes",
        "getlistwhere": "",
        "joins": [
            {
                "alias": "Location"
            },
            {
                "alias": "Address"
            },
            {
                "alias": "Person"
            },
            {
                "alias": "PersonData"
            },
            {
                "alias": "UserData"
            }
        ],
        "hooksnippets": {
            "beforesave": "migxVerifyData",
            "aftersave": "migxSaveReleafOffer"
        },
        "cmpmaincaption": "ReleafBrain",
        "cmptabcaption": "Offers",
        "cmptabdescription": "A list of goods and services that have been pledged by contributors.",
        "cmptabcontroller": "",
        "winbuttons": "",
        "onsubmitsuccess": "",
        "submitparams": ""
    },
    "columns": [
        {
            "MIGX_id": "",
            "header": "ID",
            "dataIndex": "id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Image",
            "dataIndex": "image_id",
            "width": 30,
            "sortable": false,
            "show_in_grid": 0,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[$migxGridImage? &class=`releafImageNeed` &img=`[[+Image_img]]` &parent_id=`[[+id]]` &uid=`need_[[+id]]`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Title",
            "dataIndex": "title",
            "width": 50,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Description",
            "dataIndex": "description",
            "width": 80,
            "sortable": false,
            "show_in_grid": 0,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Category",
            "dataIndex": "category",
            "width": 30,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[migxLoopCollection? &packageName=`romanescobackyard` &classname=`rmOption` &where=`[{\"alias\":\"[[+category]]\"}]` &tpl=`rawName`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Tags",
            "dataIndex": "tags",
            "width": 50,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[migxLoopCollection? &packageName=`romanescobackyard` &classname=`rmOption` &where=`[{\"id:IN\":\"[ [[+tags]] ]\"}]` &tpl=`rawName`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Quantity",
            "dataIndex": "quantity",
            "width": 30,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+quantity:append=` [[+quantity_unit]]`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Stock",
            "dataIndex": "stock",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+stock]]",
            "renderoptions": "",
            "editor": "this.textEditor"
        },
        {
            "MIGX_id": "",
            "header": "Pledged by",
            "dataIndex": "UserData_fullname",
            "width": 50,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+UserData_fullname]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Location",
            "dataIndex": "address_id",
            "width": 50,
            "sortable": false,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+Address_locality:append=`[[+Address_lgu:notempty=`, [[+Address_lgu]]`]]`:empty=`Same as contact`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Date",
            "dataIndex": "createdon",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+createdon:date=`%Y-%m-%d`]]",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Public",
            "dataIndex": "published",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderSwitchStatusOptions",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": [
                {
                    "MIGX_id": 1,
                    "name": "No",
                    "use_as_fallback": 1,
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                },
                {
                    "MIGX_id": 2,
                    "name": "Yes",
                    "use_as_fallback": "",
                    "value": 1,
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/check-square-o.svg"
                },
                {
                    "MIGX_id": 3,
                    "name": "No",
                    "use_as_fallback": "",
                    "value": "0",
                    "clickaction": "",
                    "handler": "",
                    "image": "assets\/semantic\/dist\/themes\/romanesco\/assets\/icons\/square-o.svg"
                }
            ],
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "deleted",
            "dataIndex": "deleted",
            "width": "",
            "sortable": "false",
            "show_in_grid": "0",
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        }
    ],
    "category": ""
}